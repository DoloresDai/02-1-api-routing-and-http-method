# 作业要求

## 1 Definition of Done

* 必须非常严格的按照题目中的定义进行实现。
* 每一种 case 都必须至少包含一个独立的测试。
* 所有的测试必须都成功。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。

## 2 请书写测试来验证如下的 case

### 2.1 Path Variable 的匹配

请实现 API 并匹配 *api/users/2/books* 和 *api/users/23/books*，其中 users 后面的整数应当为 64 位整数，是每一个 user 的唯一标识。该 API 的返回的响应应当为：

|项目|说明|
|---|---|
|Status Code|200|
|Content-Type|text/plain|
|Body|`The book for user $userId$`|

其中 `$userId$` 应当替换为相应的 user 的标识数字。

### 2.2 Path Variable 大小写

如果 `@PathVariable` 和 `URI` 中的名称大小写不一致可以么？写测试证明这一点。

### 2.3 优先级

如果有两个 API，均是 **GET**，其中一个 Route 为 

*/api/segments/good*

另一个为 

*/api/segments/{segmentName}*

那么如果访问 GET /api/segments/good 会调用哪一个？写测试证明这一点。

### 2.4 **?** 通配符

* 对于单字通配符的 Routing。如果 URI 相应的部分大于一个字母匹配还能成功么？书写测试证明这一点，断言并确定 Staus Code 的值。
* 对于单字通配符的 Routing。如果 URI 相应的部分一个字也不传那还能匹配成功么？书写测试证明这一点，断言并确定 Staus Code 的值。

### 2.5 **\*** 通配符

* 除了单个字符我们还可以用 \* 匹配 path 的一节。请将 \* 写在最后一个segment。例如 /api/wildcards/*。请书写测试使用上述 Route 匹配 api/wildcards/anything。
* 但是如果将 \* 写在 URI 的中间的一节可以么？请写测试证明这一点。(Todo 404)
* 如果API的定义为 /api/wildcard/before/\*/after那么如果我用 /api/before/after 请求可以匹配么？写测试证明这一点。
* 我可以使用 \* 通配符实现一节中前缀和后缀的匹配么？写测试证明这一点。

### 2.6 **\*\*** 通配符

多节通配符 ** 可以写在path的中间么？请写测试证明这一点。

### 2.7 正则表达式
除了通配符，URI template中可以使用正则表达式吗？请写测试证明这一点

### 2.8 Query String

* 如果我们希望使用 Query String 来进行 Routing该怎么办呢？写测试演示如何使用 Query String 进行 Routing。
* 如果我们声明了 `@RequestParam` 的参数但是访问时在 URI 中拒不提供，会出现什么现象呢？写测试证明。
* 如果我们希望 Routing 规则中某个特定的 Query String 键一定不能出现，该怎么写呢？如果实际的 URI 中偏偏出现了不该出现的 Query String 变量，那么响应会是什么样子的呢？写测试证明这一点。
