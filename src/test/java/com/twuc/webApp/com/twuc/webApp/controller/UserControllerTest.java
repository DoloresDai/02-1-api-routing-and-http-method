
package com.twuc.webApp.com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_user_id() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 2"));
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 23"));
    }

    @Test
    void should_compare_variable_is_different() throws Exception {
        mockMvc.perform(get("/api/users/emm/good"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_filter_gender_is_female_and_class_is_A() throws Exception {
        mockMvc.perform(get("/api/student")
                .param("gender", "female")
                .param("class", "A")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("Gender is female and class is A"));
    }

    @Test
    void should_judge_wildcard_when_routing_greater_than_two() throws Exception {
        mockMvc.perform(get("/apiiii/gender"))
                .andExpect(status().is(404));
        //TODO：404
    }

    @Test
    void should_judge_wildcard_when_routing_is_empty() throws Exception {
        mockMvc.perform(get("/apiss//"))
                .andExpect(status().is(404));
    }
    //TODO 404

    @Test
    void should_judge_wildcard_when_routing_is_single() throws Exception {
        mockMvc.perform(get("/apiss/a"))
                .andExpect(status().isOk())
                .andExpect(content().string("?"));
    }

    @Test
    void should_judge_wildcard_when_routing_is_large() throws Exception {
        mockMvc.perform(get("/api/wildcards/larrrge"))
                .andExpect(status().isOk())
                .andExpect(content().string("*"));
    }

    @Test
    void should_judge_wildcard_in_routing() throws Exception {
        mockMvc.perform(get("/api/wildcards/aaaa/larrrge"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_judge_wildcard_in_routing_and_match() throws Exception {
        mockMvc.perform(get("/api/before/after"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_judge_wildcard_in_routing_as_prefix() throws Exception {
        mockMvc.perform(get("/api/wildcard/acccb"))
                .andExpect(status().isOk())
                .andExpect(content().string("* in routing as prefix"));
    }

    @Test
    void should_judge_wildcard_when_asterisk_is_double_in_routing() throws Exception {
        mockMvc.perform(get("/apio/wildcard/name/age"))
                .andExpect(status().isOk())
                .andExpect(content().string("* is double"));
    }

    @Test
    void should_judge_regular_expression_in_routing_and_return_error() throws Exception {
        mockMvc.perform(get("/api/students/aa"))
                .andExpect(status().isOk())
                .andExpect(content().string("Wonderful!"));
    }

    @Test
    void should_use_query_string_if_one_param_is_empty_and_return_error() throws Exception {
        mockMvc.perform((get("/api/student"))//TODO /api/student?gender=male
                .param("gender","male")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void should_use_query_string_if_one_param_is_unexpected_and_return_error() throws Exception {
        mockMvc.perform((get("/api/student"))
                .param("gender","male")
                .param("name","Dolores")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError());
    }
}

