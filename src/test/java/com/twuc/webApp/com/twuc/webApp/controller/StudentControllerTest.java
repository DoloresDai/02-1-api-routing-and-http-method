package com.twuc.webApp.com.twuc.webApp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.com.twuc.webApp.controller.com.twuc.webApp.model.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_return_costumed_ResponseEntity() throws Exception {
        mockMvc.perform(post("/api/students"))
                .andExpect(status().isCreated())
                .andExpect(header().string("id", "100"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("success"));
    }

    @Test
    void should_return_json_lack_of_one_property() throws Exception {
        Student student = new Student("xiaoming",123);
        ObjectMapper objectMapper = new ObjectMapper();
/*        objectMapper.writeValueAsString(student);
        assertEquals("xiaoming",student.getName());
        assertEquals(123,student.getPhoneNumber());*/
        mockMvc.perform(post("/api/student")
        .content("{\"name\":\"xiaoming\",\"phone_number\":123}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("This student is xiaoming and his phone number is 123"));
    }
}
