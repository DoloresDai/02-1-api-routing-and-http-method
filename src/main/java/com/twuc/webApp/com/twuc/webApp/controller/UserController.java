package com.twuc.webApp.com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @GetMapping("api/users/{id}/books")
    String getUserId(@PathVariable long id) {
        return "The book for user " + id;
    }

    @GetMapping("api/segments/good")
    String getApiCorrectFirst() {
        return "The message is good";
    }

    @GetMapping("api/segments/{segments}")
    String getSegement(@PathVariable String segments) {
        return "The message is " + segments;
    }

    @GetMapping("/api/student")
    String getGenderAndClass(@RequestParam String gender, @RequestParam(name = "class") String klass) {
        return "Gender is " + gender + " and class is " + klass;
    }

    @GetMapping("/apiiii/student")
    void getGenderAndClass(){
    }

    @GetMapping("apiss/?")
    String useWildcardQuestionMark() {
        return "?";
    }

    @GetMapping("api/wildcards/*")
    String useWildcardAsterisk() {
        return "*";
    }

    @GetMapping("/api/wildcard/before/*/after")
    String useWildcardAsteriskInRouting() {
        return "* in routing";
    }

    @GetMapping("/api/wildcard/a*b")
    String useWildcardAsteriskAsPrefix() {
        return "* in routing as prefix";
    }

    @GetMapping("/apio/**")
    String useWildcardAsteriskIsDouble() {
        return "* is double";
    }

    @GetMapping("/api/students/{[abc]+}")
    String useRegularExpression(){
        return "Wonderful!";
    }
}
//
//
//
