package com.twuc.webApp.com.twuc.webApp.controller;

import com.twuc.webApp.com.twuc.webApp.controller.com.twuc.webApp.model.Student;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
    @PostMapping("/api/students")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity costumedResponseEntity() {
        return ResponseEntity.created(null)
                .header("id", "100")
                .contentType(MediaType.APPLICATION_JSON)
                .body("success");
    }

    @PostMapping("/api/student")
    public String returnStudentLackOfProperty(@RequestBody Student student) {
        return "This student is " + student.getName() + " and his phone number is " + student.getPhoneNumber();
    }
}
