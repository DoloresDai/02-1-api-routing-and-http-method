package com.twuc.webApp.com.twuc.webApp.controller.com.twuc.webApp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Student {
    private String name;
    private int age;
    @JsonProperty("phone_number")
    private int phoneNumber;

    public Student() {
    }

    public Student(String name, int age, int phoneNumber) {
        this.name = name;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }

    public Student(String name, int phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
